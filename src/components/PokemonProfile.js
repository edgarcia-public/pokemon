import "../assets/progressBar.css";

const PokemonProfile = ({ pokeBasic, pokeSpecie }) => {
	return (
		<div className="pokemonPage-wrapper">
			<div className="pokemonPage--info flexbox">
				<h1 className="pokemonPage--name">{pokeBasic.name}</h1>
				<h3 className="pokemonPage--expi">EXPI {pokeBasic.base_experience}</h3>
			</div>
			<div className="flexbox item-inherit">
				<div className="flex-item right-stats">
					<p className="label">Description:</p>
					{pokeSpecie?.flavor_text_entries ? pokeSpecie.flavor_text_entries.slice(0, 5).map((item, index) => <p key={index}>{item.flavor_text.replace("", " ").trim()}</p>) : <p>Loading...</p>}
				</div>
				<div className="flex-item center-stats">
					<p className="label">Sprites Default</p>
					<div className="pokemonPage--sprites">
						<img src={pokeBasic.sprites.front_default} alt="" />
						<img src={pokeBasic.sprites.back_default} alt="" />
					</div>
					<p className="label">Sprites Shiny</p>
					<div className="pokemonPage--sprites">
						<img src={pokeBasic.sprites.front_shiny} alt="" />
						<img src={pokeBasic.sprites.back_shiny} alt="" />
					</div>
				</div>
			</div>

			<div className="flexbox item-inherit">
				<div className="flex-item">
					<p className="label">Abilities</p>
					<div className="flexbox">
						{pokeBasic.abilities.map((item, index) => <p key={index} className="text-capital">{item.ability.name}</p>)}
					</div>
					<p className="label">Abilities</p>
					<div className="flexbox">
						{pokeBasic.held_items.map((item, index) => <p key={index} className="text-capital">{item.item.name}</p>)}
					</div>
				</div>
				<div className="flex-item">
					<p className="label">Profile</p>
					<div className="flexbox">
						<p>Height</p>
						<p>{pokeBasic.height}"</p>
					</div>
					<div className="flexbox">
						<p>Weight</p>
						<p>{pokeBasic.weight}lbs</p>
					</div>
				</div>
			</div>
		</div>
	)
}

export default PokemonProfile;