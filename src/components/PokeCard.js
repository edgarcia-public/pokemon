import "../assets/card.css";
import bgData from "../assets/bgData";
import { NavLink } from "react-router-dom";

const PokeCard = ({ poke }) => {

	const bg = bgData[poke.types[0].type.name];

	return (
		<div className="card" id={`${poke.name}_${poke.types[0].type.name}`}>
			<div className="card--pokemon-id"><b>#</b> {poke.id}</div>
			<div className="card--inner" style={{ backgroundImage: `url(${require(`../assets/images/${bg ? bg : "default.jpg"}`)})` }}>
				<div className="card--header">
					<div className="header--profile">
						<h1 className="profile--title">{poke.name}</h1>
						<h1 className="profile--title">XP {poke.base_experience}</h1>
					</div>
					<div className="header--image-wrapper"><img src={poke.sprites.front_default ? poke.sprites.front_default : require("../assets/images/icons/pokeball.png")} className="card-image" alt="" /></div>
				</div>
				<div className="card--main">
					<div className="main--physical align-center">
						<div className="capsule">Height: <b>{poke.height}"</b></div>
						<div className="capsule">Weight: <b>{poke.weight}</b> lbs</div>
					</div>
				</div>
				<hr />
				<div className="card--footer">
					<div className="footer--abilities align-center">
						<p className="label">Abilities</p>
						<div className={`capsule ${poke.types[0].type.name}`}><b>{poke.abilities[0].ability.name}</b></div>
						{poke.abilities[1] ? <div className={`capsule ${poke.types[0].type.name}`}><b>{poke.abilities[1].ability.name}</b></div> : ""}
					</div>

					<NavLink className="btn-stats" to={`/types/${poke.types[0].type.name}/${poke.name}`}>More Info</NavLink>
				</div>
			</div>
		</div>
	)
}

export default PokeCard;