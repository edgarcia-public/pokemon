import { NavLink } from "react-router-dom";
import typesData from "../assets/typesData";
import "../assets/cardType.css";

const CardType = ({ item }) => {
	return (
		<div className="cardType">
			<div className="cardType--info">
				<NavLink to={item.name} className="cardType--hero">
					<img src={require(`../assets/images/icons/${item.name}.png`)} width="40" alt="" />
					<h1 className="cardType--title">{item.name}</h1>
				</NavLink>
				<p className="cardType--desc">{typesData[item.name]}</p>
			</div>
			<div className="cardType--panel">
				<p className="cardType--label">Top moves:</p>
				<ul className="cardType--list">
					{item.moves.slice(0, 6).map((move, index) => <li key={index}>{move.name}{index !== 5 ? "," : ""} {index === 4 ? <span style={{ textTransform: "lowercase" }}>and</span> : ""} </li>)}
				</ul>
			</div>
			{item.pokemon.length ? <div className="cardType--panel">
				<p className="cardType--label">Used by:</p>
				<ul className="cardType--list">
					{item.pokemon.slice(4, 10).map((item, index) => <li key={index}>{item.pokemon.name}{index !== 5 ? "," : ""} {index === 4 ? <span style={{ textTransform: "lowercase" }}>and</span> : ""}</li>)}
				</ul>
				<NavLink className="more-btn" to={item.name}>More Pokemon +{item.pokemon.length}</NavLink>
			</div>
				: ""}
		</div>
	)
}

export default CardType;