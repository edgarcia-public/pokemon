import logo from "../assets/images/pokemon_logo.png"

const Header = () => {
	return (
		<header id="header">
			<img src={logo} className="logo" alt="Pokemon Logo" />
		</header>
	)
}

export default Header;