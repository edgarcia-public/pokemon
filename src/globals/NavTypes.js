import { NavLink, Outlet, useLoaderData } from "react-router-dom";

const NavTypes = () => {
	const fetchTypes = useLoaderData();

	const theTypes = fetchTypes.results.map(result => result.name).filter(type => type !== "unknown" && type !== "shadow");
	theTypes.unshift("home");

	const showNavLinks = theTypes.map((type, index) =>
		<NavLink
			key={index}
			to={type === "home" ? "/" : `/types/${type}`}
			className={({ isActive }) => isActive ? "navlink active" : "navlink"}
		>
			<img src={require(`../assets/images/icons/${type}.png`)} alt="" />
			<p>{type}</p>
		</NavLink>);

	return (
		<>
			<nav className="navtypes">
				<div className="navtypes--wrapper">
					{showNavLinks}
				</div>
			</nav>

			<Outlet />
		</>
	)
}

export default NavTypes;