import { NavLink, useParams } from "react-router-dom";
import "../assets/global.css";
import typesData from "../assets/typesData";

const Breadcrumbs = (props) => {

	const urlParams = useParams();
	const temp = [];
	let linkArray = [];

	if (props.links) {
		props.links.map(link => temp.push(link));
	}

	if (Object.keys(urlParams)) {
		Object.keys(urlParams).map(key => temp.push(urlParams[key]));
		temp.unshift("/");
	}

	if (typesData[temp[1]] && temp[2]) {
		linkArray = ["/", `/types/${temp[1]}`, `/types/${temp[1]}/${temp[2]}`]
	} else {
		if (temp[2]) {
			linkArray = ["/", `/types`, `/types/${temp[2]}`]
		} else {
			linkArray = ["/", `/types`]
		}
	}

	const showBreadcrumbs = linkArray.map((link, index) =>
		<div key={index} className="breadcrumb--parent">
			<NavLink to={link} className="breadcrumb--btn">
				{temp[index] === "/" ? "home" : temp[index]}
			</NavLink>
			<img src={require("../assets/images/arrow-right-theme.png")} width="15" alt="" />
		</div >)

	return (
		<nav id="navHome">
			{showBreadcrumbs}
		</nav>
	)
}

export default Breadcrumbs;