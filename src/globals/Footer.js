import loading from "../assets/images/loading.gif";

const Footer = ({ isLoading, handleFooterBtn }) => {

	return (
		<footer id="footer">
			<div className="container">
				<div className="button-group">
					<button id="next_btn" disabled={isLoading} onClick={e => handleFooterBtn(e.target.id)} style={{ textAlign: "center", display: "block" }}>
						{isLoading ? <><img src={loading} alt="" width="15" /> Loading...</> : "Load More"}
					</button>
				</div>
			</div>
		</footer>
	)
}

export default Footer;