const bgData = {
	bug: "bug.jpg",
	default: "default.jpg",
	electric: "electric.jpg",
	fairy: "fairy.jpg",
	fire: "fire.jpg",
	flying: "flying.jpg",
	ghost: "ghost.jpg",
	grass: "grass.jpg",
	ground: "ground.jpg",
	ice: "ice.jpg",
	loading: "loading.jpg",
	poison: "poison.jpg",
	psychic: "psychic.jpg",
	skyblue: "skyblue.jpg",
	steel: "steel.jpg",
	water: "water.jpg"
};

export default bgData;