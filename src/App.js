import "./assets/global.css";
import Header from "./globals/Header";
import Home from "./pages/Home";
import Breadcrumbs from "./globals/Breadcrumbs";
import NavTypes from "./globals/NavTypes";
import PokemonTypes from "./pages/PokemonTypes";
import PokemonPage from "./pages/PokemonPage";
import PokemonList from "./pages/PokemonList";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

const App = () => {
	const loaderFetchTypes = async (type) => {
		return fetch(`https://pokeapi.co/api/v2/type/${type && ""}`);
	}

	const router = createBrowserRouter([
		{
			path: "",
			loader: loaderFetchTypes,
			element: <NavTypes />,
			children: [
				{
					path: "",
					element: <Home />
				}
			]
		},
		{
			path: "/types",
			loader: loaderFetchTypes,
			element: <PokemonTypes />,
			children: [
				{
					path: "",
					element: <Breadcrumbs links={["types"]} />,
					children: [

					]
				},
			],
		},
		{
			path: "/types/:typeID",
			loader: loaderFetchTypes,
			element: <PokemonList />,
			children: [
				{
					path: "/types/:typeID",
					element: <Breadcrumbs links={["types"]} />
				},
			]
		},
		{
			path: "/types/:typeID/:pokeName",
			loader: loaderFetchTypes,
			element: <PokemonPage />,
			children: [
				{
					path: "",
					element: <Breadcrumbs />
				}
			]
		},
	])

	return (
		<div className="container">
			<Header />
			<RouterProvider router={router} />
		</div>
	)
}

export default App;