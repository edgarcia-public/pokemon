import { useEffect, useState } from "react";
import { Outlet, useLoaderData, useParams } from "react-router";
import PokeCard from "../components/PokeCard";
import Footer from "../globals/Footer";

const PokemonList = () => {
	const [raw, setRaw] = useState([]);
	const [pokeProfile, setPokeProfile] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [offset, setOffset] = useState(20);
	const { typeID } = useParams();
	const fetchTypes = useLoaderData();

	document.title = `Pokemon Type: ${typeID}`;

	const handleFooterBtn = (e) => {
		setOffset(offset + 20)
	}

	useEffect(() => {
		const theUrls = fetchTypes.results.map(result => result.url);

		Promise.all(theUrls.map(url => fetch(url)))
			.then(responses => Promise.all(responses.map(response => response.json())))
			.then(data => setRaw(data.filter(item => item["name"] === typeID).map(item => item.pokemon)[0]))
			.catch(error => console.log(error))
	}, [fetchTypes.results, typeID]);


	useEffect(() => {
		const pokeData = raw.map(item => item);
		const pokeNames = pokeData.map(item => item.pokemon.name);

		let resultCount = "";

		Promise.all(pokeNames.map(name => fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)))
			.then(responses => Promise.all(responses.map(response => {
				resultCount = responses.length;
				return response.json();
			})))
			.then(data => {
				if (data.length === resultCount) {
					setPokeProfile(data);
					setIsLoading(false);
				}
			})
			.catch(error => console.log(error))
	}, [raw]);

	const uiOffset = pokeProfile.slice(0, offset);
	const showUI = uiOffset.map((poke, index) => <PokeCard key={index} poke={poke} />);

	return (
		<>
			<div id="pokemonList" className="container">
				<Outlet />

				<section id="pokedex">
					{showUI}
				</section>

				{isLoading ? <center><h4>Loading...</h4><img src={require("../assets/images/icons/poke-loading.gif")} width="20%" alt="" /></center> : ""}
			</div>

			<Footer isLoading={isLoading} handleFooterBtn={handleFooterBtn} />
		</>
	)
}

export default PokemonList;