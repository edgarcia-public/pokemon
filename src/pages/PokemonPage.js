import { useEffect, useState } from "react";
import { Outlet, useParams } from "react-router"
import PokemonProfile from "../components/PokemonProfile";
import "../assets/pokemonPage.css";

const PokemonPage = () => {
	const [pokeBasic, setPokeBasic] = useState([]);
	const [pokeSpecie, setPokeSpecie] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const { pokeName } = useParams();

	document.title = `Pokemon Profile : ${pokeName}`

	useEffect(() => {
		const fetchPokemon = async () => {
			try {
				const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokeName}`);
				const data = await response.json();

				if (response.ok) {
					setPokeBasic(data);
					setTimeout(() => {
						setIsLoading(false);
					}, 1000);
				}
			} catch (error) {
				console.log(error);
			}
		}

		fetchPokemon();
	}, [pokeName]);

	useEffect(() => {
		const fetchSpecie = async () => {
			try {
				const response = await fetch(`https://pokeapi.co/api/v2/pokemon-species/${pokeName}`)
				const data = await response.json();

				if (response.ok) {
					setPokeSpecie(data);
					setTimeout(() => {
						setIsLoading(false);
					}, 800);
				}
			} catch (error) {
				console.log(error);
			}
		}

		fetchSpecie();
	}, [pokeBasic, pokeName])

	const showUI = pokeBasic ? <PokemonProfile pokeBasic={pokeBasic} pokeSpecie={pokeSpecie} /> : "";

	return (
		<div id="pokemonPage">
			<Outlet />
			{isLoading ?
				<center>
					<h4>Loading...</h4>
					<img src={require("../assets/images/icons/poke-loading.gif")} width="300" alt="" />
				</center>
				:
				showUI
			}
		</div>
	)
}

export default PokemonPage;