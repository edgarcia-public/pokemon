import { useState, useEffect } from "react";
import PokeCard from "../components/PokeCard";
import Footer from "../globals/Footer";

document.title = "Pokemon | By Ed";

const Home = () => {
	const [offset, setOffset] = useState(0);
	const [isLoading, setIsLoading] = useState(true);
	const [pokeNames, setPokeNames] = useState([]);
	const [pokeProfile, setPokeProfile] = useState([]);

	const handleFooterBtn = (e) => {
		setOffset(offset + 20);
		setIsLoading(true);
	}

	useEffect(() => {
		(async () => {
			const response = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=20&offset=${offset}`);
			const data = await response.json();
			const result = data.results.map(result => result.name);

			setPokeNames(result);
		})()

	}, [offset]);

	useEffect(() => {
		if (pokeNames.length) {
			const fetchPokeNames = pokeNames.map(
				name => fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
					.then(response => response.json())
			)

			Promise.all(fetchPokeNames)
				.then(data => {
					setPokeProfile(prev => [...prev, ...data]);
					setTimeout(() => {
						setIsLoading(false);
					}, 300)

					window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: "smooth" });
				})
				.then(error => console.log(error))
		}

	}, [pokeNames]);

	const showUI = pokeProfile.map((poke, index) => <PokeCard key={index} poke={poke} />);

	return (
		<>
			<main id="main">
				<section id="pokedex">
					{showUI}
				</section>

				{isLoading ? <center><h4>Loading...</h4><img src={require("../assets/images/icons/poke-loading.gif")} width="20%" alt="" /></center> : ""}
			</main>

			<Footer isLoading={isLoading} handleFooterBtn={handleFooterBtn} />
		</>
	)
}

export default Home;