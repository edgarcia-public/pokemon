import { useEffect, useState } from "react";
import { Outlet, useLoaderData } from "react-router";
import CardType from "../components/CardType";

const PokemonTypes = () => {
	document.title = `All Pokemon Types`;

	const [uiData, setUiData] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	const fetchTypes = useLoaderData();

	useEffect(() => {
		const theUrls = fetchTypes.results.map(result => result.url);
		let resultCount = "";

		Promise.all(theUrls.map(url => fetch(url)))
			.then(responses => Promise.all(responses.map(response => {
				resultCount = responses.length;
				return response.json();
			})))
			.then(data => {
				if (data.length === resultCount) {
					setUiData(data.filter(item => item.name !== "unknown"));
					setIsLoading(false);
				}
			})
			.catch(error => console.log(error))

	}, [fetchTypes.results]);

	const showUI = uiData.map((item, index) => <CardType key={index} item={item} />)

	return (
		<div id="pokemonTypes" className="container">
			<Outlet />
			<div className="cardType-wrapper">
				{showUI}
			</div>

			{isLoading ? <center><h4>Loading...</h4><img src={require("../assets/images/icons/poke-loading.gif")} width="20%" alt="" /></center> : ""}
		</div>
	)

}

export default PokemonTypes;